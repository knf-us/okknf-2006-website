<?php
	include_once("header.php");
	include_once("footer.php");
	p_header("PIKNIK NAUKOWY 2006 -- Miejsce konferencji", "cieszyn");
?>

<p>
	<span class="header">Miejsce konferencji - o Cieszynie</span>

<img src="images/herb_cieszyn.png" alt="Herb Cieszyna" class="imgRight" />

Malowniczo położony u podnóża Beskidu Śląskiego, nad rzeką Olzą, Cieszyn jest historyczną stolicą regionu. Podanie 
głosi, że w 810 roku trzej bracia: Cieszko, Bolko i Mieszko, spotkali się przy tutejszym źródle po długiej wędrówce i 
ciesząc się tym faktem założyli miasto Cieszyn. Do dzisiaj Studnia Trzech Braci ma upamiętniać to wydarzenie. Badania 
naukowe stwierdzają jednak, że historia cieszyńskiego grodu sięga znacznie dalej, niż głosi podanie. </p>

<p>Obecnie miasto podzielone jest wzdłuż Olzy na części polską i czeską, przy czym historyczna
część miasta pozostała po stronie polskiej. Przez długie lata Cieszyn był jednym z najważniejszych grodów w tej części 
Europy, jednak w XX w. utracił swą dominującą pozycję w regionie na rzecz przemysłowych Bielska i Ostrawy, co 
utrwalił jeszcze podział miasta w 1920 roku, który sprowadził Cieszyn do roli miasta granicznego.</p>

<p>Wichry XX-wiecznych wojen oszczędziły ten stary piastowski gród, co przy niewielkim stopniu uprzemysłowienia i 
rozbudowy pozwoliło zachować Cieszynowi jego specyficzny charakter i klimat. Składa się na nie zarówno piękno 
starego miasta, z niezliczonymi zabytkami, jak i stosunkowo duża ilość rdzennej ludności, często rozpamiętującej dawną
świetność i niezależność swego miasta i regionu.</p>

<p>Nazywany "małym Wiedniem" Cieszyn jest podstawowym punktem programu każdego szanującego się turysty 
przebywającego w okolicy. Mimo niewielkich rozmiarów miasto pozostało ważnym ośrodkiem kulturalnym, oświatowym 
i administracyjnym, a część zabytkowa miasta może ująć naprawdę każdego...<br /><br />

<a href="http://www.ksiestwocieszynskie.republika.pl/cieszyn.html">Żródło</a></p>

<p>
	<span class="header">Zabytki</span>

<img src="images/cieszyn_rotunda.jpg" alt="Cieszyńska Rotunda" class="imgLeft" />

Okolice, jak i sam Cieszyn, posiadają wiele zabytków. W tym roku logiem konferencji jest jeden z nich - 
Rotunda romańska z XI w. w kościele św. Mikołaja i św. Wacława.<br /><br />

<a href="http://www.ksiestwocieszynskie.republika.pl/zabcieszyn.html">Więcej o zabytkach</a>
</p>

<p><span class="header">Dojazd</span>
Do Cieszyna najłatwiej dostać się pociągiem. Istnieje kilka dogodnych połączeń:
</p>

<ul>
<li>Katowice - Cieszyn (przez Zebrzydowice)</li>
<li>Kraków - Cieszyn (przez Czechowice-Dziedzice)</li>
<li>Warszawa - Cieszyn (przez Czechowice-Dziedzice)</li>
<li>Poznań/Wrocław - Cieszyn (przez Katowice)</li>
<li>Zielona Góra - Cieszyn (przez Katowice)</li>
</ul>

<p>Więcej informacji na stronie <a href="http://rozklad.pkp.pl/bin/query.exe/pn?ld=pkp">PKP</a>.</p>

<p><span class="header">Plan miasta</span></p>

<p style="text-align:center;">
	<img src="files/mapka_zakw.png" /><br />
Na planie zaznaczono miejsca zakwaterowania uczestników konferencji oraz
dworzec PKP. <br />Plan opracowany na podstawie internetowego planu Cieszyna
z <a href="http://www.cieszyn.pl/">http://www.cieszyn.pl/</a>.
</p>

<p>
<span class="header">Linki</span>

<a href="http://www.cieszyn.pl/">Strona WWW Cieszyna</a>

</p>

<!--
<p>
	<span class="header">Dojazd i komunikacja</span>

</p>
-->

<?php
	p_footer();
?>
