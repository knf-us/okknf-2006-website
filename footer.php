<?php
	require_once("settings.php");

function p_footer() 
{
	global $root_uri;

	echo <<<HTML
		<img src="$root_uri/images/corner_sub_bl.gif" alt="bottom corner" class="vBottom"/>

    </div>

  </div>

  <div id="footer">
    <div id="footerLeft">&nbsp;</div>
	<div id="footerRight">&nbsp;</div>
	
	<a href="http://validator.w3.org/check?uri=referer"><img alt="Valid XHTML 1.1" src="$root_uri/images/icons/xhtml.png" /></a>
	<a href="http://jigsaw.w3.org/css-validator/check/referer"><img alt="Valid CSS" src="$root_uri/images/icons/css.png" /></a>
	<a href="http://www.vim.org/"><img alt="VIM" src="$root_uri/images/icons/vim.png" /></a>
	<a href="http://www.gentoo.org/"><img alt="Gentoo" src="$root_uri/images/icons/gentoo.png" /></a>
	<a href="http://www.mozilla.org/products/firefox"><img alt="Firefox" src="$root_uri/images/icons/firefox.png" /></a>
	<a href="http://www.gimp.org/"><img alt="The GIMP" src="$root_uri/images/icons/gimp.png" /></a>
	<a href="http://www.nosoftwarepatents.com/en/m/intro/index.html"><img alt="No Software Patents" src="$root_uri/images/icons/noepatents.png" /></a>
	<a href="http://www.againsttcpa.com/"><img alt="Against-TCPA" src="$root_uri/images/icons/tcpa.png" /></a>
	<a href="http://www.bykom-stop.avx.pl/"><img alt="Bykom Stop!" src="$root_uri/images/icons/bykomstop.png" /></a><br />
	design based on <a href="http://oswd.org/userinfo.phtml?user=snop" title="OSWD design work">snop's</a> template
  </div>

</div>

</body>

</html>
HTML;

}

?>
