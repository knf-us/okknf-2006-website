<?php
	ob_start();
	require_once("settings.php");
		
function p_header($title, $caller) {
	global $root_uri;
	
	$menu = array(
				"cieszyn"	=> array("cieszyn.php", "cieszyn", "miejsce konferencji", "dojazd, o Cieszynie"),
				"info"		=> array("info.php", "informacje", "informacje", "organizatorzy i sponsorzy"),
				"konf"		=> array("konf.php", "konferencja", "konferencja", "rejestracja, uczestnicy, program i plan"),
				"mesgs"		=> array("komunikaty.php", "komunikaty", "komunikaty", "zakwaterowanie, opłaty, pliki"),
				"main"		=> array("index.php", "informacje ogólne", "strona główna", "informacje ogólne")
			);
	
	echo <<<HTML
	
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en-AU">
<!--
 _________________________________________________________
|                                                         |
|    DESIGN + http://fullahead.org                        |
|      DATE + 2005.05.12                                  |
| COPYRIGHT + free use if this notice is kept in place    |
|_________________________________________________________|

-->
<head>
  <meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8" />
  <meta name="author" content="fullahead.org" />
  <meta name="keywords" content="reflection, fullahead, pat, OSWD, more, keywords, separated, with, commas" />
  <meta name="description" content="Strona V Ogólnopolskiej Konferencji Kół Naukowych Fizyków PIKNIK NAUKOWY 2006 - 20-23 kwietnia 2006 Cieszyn" />
  <title>$title</title>
  <link rel="stylesheet" type="text/css" href="$root_uri/css/screen_yellow.css" media="screen, tv, projection" />
</head>

<body>

<div id="siteBox">
  <div id="header">
    <img src="$root_uri/images/corner_tl.gif" alt="corner" style="float:left;" />

	<span class="title">
      <span class="white">PIKNIK NAUKOWY</span>2006
	  <span class="subTitle">www.knf.us.edu.pl/konf2006</span>
    </span>

    <!-- Menu is displayed in reverse order from how you define it (caused by float: right) -->
HTML;
	$i = 0;
	foreach (array_keys($menu) as $id) {
		
		echo '<a href="'.$root_uri.'/'.$menu["$id"][0].'" title="'.$menu["$id"][1].'"';
		
		if ($id == $caller) {
			echo ' class="active"';
		} else if ($i == 0) {
			echo ' class="lastMenuItem"';
		}

		echo '>'.$menu["$id"][2].'<span class="desc">'.$menu["$id"][3].'</span></a>';
		echo "\n";		
		$i++;	
	}

	echo <<<HTML
  </div>

  <div id="content">
	<div id="contentLeft">

      <p>
        <span class="header">Patronat honorowy</span>
	  </p>

	  <ul>
		<li>JM Rektor Uniwersytetu Śląskiego w Katowicach <b>prof. dr hab. Janusz Janeczek</b></li>
		<li>Prezes Polskiego Towarzystwa Fizycznego <b>prof. dr hab. Reinhard Kulessa</b></li>
		<li>Burmistrz Miasta Cieszyna <b>dr inż. Bogdan Ficek</b></li>
	  </ul>
 
	  <p>
        <span class="header">Organizatorzy</span>
		Organizatorami Konferencji są:
      </p>

      <p>
		<a href="http://knf.us.edu.pl/" title="KNF" class="menuItem">Koło Naukowe Fizyków Uniwersytetu Śląkiego w Katowicach</a>
		<a href="http://smptf.phys.us.edu.pl/" title="SMPTF" class="menuItem">Sekcja Młodych Polskiego Towarzystwa Fizycznego</a>
      </p>

      <div class="bottomCorner">
        <img src="$root_uri/images/corner_sub_br.gif" alt="bottom corner" class="vBottom"/>
      </div>

    </div>

    <div id="contentRight">
HTML;

}
	
