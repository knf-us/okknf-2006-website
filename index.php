<?php
	include("header.php");
	include("footer.php");
	p_header("PIKNIK NAUKOWY 2006 -- Informacje ogólne", "main");
?>

<p>

	<span class="header">informacje ogólne</span>
	Serdecznie zapraszamy do udziału w <span class="comment" title="Konferencja obchodzi w tym roku jubileusz.">
	V Ogólnopolskiej Konferencji Kół Naukowych Fizyków "Piknik Naukowy 2006"</span>, która w tym roku organizowana 
	jest w <span class="comment" title="W Cieszynie mieści się filia Uniwersytetu Śląskiego">Cieszynie</span>, 
	w dniach <b>20-23 kwietnia 2006 roku</b>.
</p>

<p>
Podczas konferencji odbywają się m.in. wykłady znakomitych przedstawicieli polskiej nauki,
jak również seminaria wygłaszane przez uczestników konferencji -- studentów, którzy 
mają możliwość zaprezentowania swoich osiągnięć oraz zainteresowań. Tradycyjnie 
podczas konferencji przewidziany jest wieczorek integracyjny mający na celu 
nawiązanie bliższej znajomości oraz współpracy między pracownikami naukowymi a 
studentami.
</p>

<p>
Rezultatem organizowanej konferencji ma być przede wszystkim możliwość 
zapoznania się z dorobkiem naukowym innych Kół Naukowych Fizyków z całego 
kraju. Konferencja ma również umożliwić rozwijanie swoich zainteresowań 
wybitnym studentom fizyki.
</p>

<p>
Mamy nadzieję, że konferencja będzie okazją do zdobycia nowych doświadczeń 
naukowych oraz do zwiedzenia wspaniałego
<span class="comment" title="Jednego ze starszych miast w Polsce, założonego ok. 810 roku">miasta</span>.
</p>

<p style="text-align:center;">
	<a href="files/Piknik_Naukowy_2006.swf" target="_blank">
		<img src="<?php echo $root_uri ?>/images/logo.png" alt="Logo Konferencji" /><br />
		Kliknij na logo konferencji aby zobaczyć animację  reklamową.
	</a>
</p>

<!--

<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,29,0" width="550" height="400">
	<param name="movie" value="images/konf2006.swf">
	<param name="quality" value="high">
	<embed src="images/konf2006.swf" quality="high" pluginspage="http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash" width="550" height="400"></embed>
</object>
-->

<!--
<object type="application/x-shockwave-flash" data="images/konf2006.swf">
	<embed src="images/konf2006.swf" width="550" height="400">	</embed>
	<param name="movie" value="images/konf2006.swf" />
</object>
-->


<?php
	p_footer();
?>
