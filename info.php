<?php
	include_once("header.php");
	include_once("footer.php");
	p_header("PIKNIK NAUKOWY 2006 -- Informacje", "info");
?>

<!--
<p>
	<span class="header">Informacje</span>
</p>
-->

<p>
	<span class="header">Komitet organizacyjny</span>
Organizatorami Konferencji są: 
<a href="http://knf.us.edu.pl/">Koło Naukowe Fizyków Uniwersytetu Śląskiego w Katowicach</a>
oraz Sekcja Młodych Polskiego Towarzystwa Fizycznego. Komitet organizacyjny:
</p>

<h2>Główny Komitet Organizacyjny</h2>
<ul>
	<li>Andrzej Ptok - prezes KNF UŚ (2005/2006), wiceprzewodniczący SM PTF</li>
	<li>Agnieszka Grzanka - viceprezes KNF UŚ ds. finansowych</li>
	<li>Artur Fijałkowski</li>
</ul>

<h2>Wykonawczy Komitet Organizacyjny</h2>
<ul>
	<li>Katarzyna Bartuś - viceprezes KNF UŚ ds. naukowych</li>
	<li>Michał Januszewski - prezes KNF UŚ (2006/2007)</li>
</ul>

<p>
Pytania i ew. zgłoszenia udziału prosimy kierować na adres e-mailowy: 
<a href="mailto:agrzanka@us.edu.pl">agrzanka@us.edu.pl</a>.<br />

W przypadku zgłoszeń
na konferencję preferowana jest rejestracja <a href="<?php echo $root_uri; ?>/reg/">on-line</a>.
</p>


<p>
	<span class="header">Sponsorzy</span>
</p>

<ul>
	<li><a href="http://www.us.edu.pl/">Uniwersytet Śląski w Katowicach</a></li>
	<li><a href="http://ptf.fuw.edu.pl/">Polskie Towarzystwo Fizyczne</a></li>
</ul>

<?php
	p_footer();
?>
