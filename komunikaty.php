<?php
	include_once("header.php");
	include_once("footer.php");
	p_header("PIKNIK NAUKOWY 2006 -- Konferencja", "mesgs");
?>

<p>
	<span class="header">Komunikaty</span>
</p>

<ul>
<li><b>UWAGA:</b> Ostateczny termin rejestracji i uiszczenia całości opłaty konferencyjnej mija 14. kwietnia!</li>
<li>Prosimy o wpłaty <b>całości opłaty konferencyjnej</b> na konto: ING BANK ŚLĄSKI, <span class="nowrap">74 1050 1214 1000 0007 0000 7909</span>, z dopiskiem: <b>"KNF-PIKNIK2006"</b>.
</li><li>Jeśli przelewu na konto dokona uczelnia, Uniwersytet Śląski automatycznie wystawi fakturę, która
zostanie wysłana do wpłacającego listem poleconym. Osoby fizyczne dokonujące wpłat i chcące otrzymać
fakturę proszone są o kontakt mailowy na adres <a href="mailto:agrzanka@us.edu.pl">agrzanka@us.edu.pl</a>.
W miarę możliwości, faktury dla osób fizycznych zostaną wystawione podczas konferencji.</li>

<li>Jedno koło naukowe może na konferencji reprezentować maksymalnie 7 osób, z czego przynajmniej jedna powinna wygłosić wykład.</li>
<!-- <li>Prosimy o wpłaty zaliczek w wysokości 75 zł za osobę (do końca marca) na konto UŚ Cieszyn DSN, <span class="nowrap">12 1050 1214 1000 0007 0000 7958.</span></li> -->
<li>Przypominamy o potrzebie podania NIP-u oraz adresu uczelni w celu wystawienia faktury VAT oraz okazania podczas konferencji
podtwierdzeń wpłaty opłat konferencyjnych oraz zaliczek.</li>
</ul>

<p>
	<span class="header">Zakwaterowanie i opłaty</span>
</p>

<h2>Miejsce konferencji i zakwaterowanie</h2>
<ul>
	<li>Wykłady, dyskusje: <br />
		<b>Centrum Konferencyjne w Cieszynie</b><br />
		43 - 400 Cieszyn, ul. Bielska 62<br />
		tel. 033 - 854 61 52<br />
		fax. 033 - 854 63 21<br /><br />
	</li>	

	<li>Zakwaterowanie (kadra): <br />
	<b>Zajazd Academicus</b><br />
	43 - 400 Cieszyn, ul. Paderewskiego 6<br />
	tel. 033 - 8546464<br />
	fax: 033 - 8546466<br /><br />
	</li>
	
	<li>Zakwaterowanie (studenci): <br />
	<b>DSN -- akademik</b><br />
	43-400 Cieszyn, ul. Niemcewicza 8<br />
	tel. 0338546130 - kierowniczka osiedla<br />
	tel./fax. 0338546127 - administracja<br />
	tel. 0338546120 - recepcja<br />
	</li>
</ul>

<h2>Opłaty</h2>

<ul>
	<li>studenci: 150 PLN</li>
	<li>pracownicy naukowi: 270 PLN</li>
</ul>

<p>W opłatę konferencyjną wliczony jest koszt noclegu w akademiku oraz wyżywienia.</p>

<p><span class="header">Plan miasta</span></p>

<p style="text-align:center;">
	<img src="files/mapka_zakw.png" /><br />
Na planie zaznaczono miejsca zakwaterowania uczestników konferencji oraz
dworzec PKP. <br />Plan opracowany na podstawie internetowego planu Cieszyna
z <a href="http://www.cieszyn.pl/">http://www.cieszyn.pl/</a>.
</p>


<p>
	<span class="header">Pliki</span>
</p>

<ul>
	<li><a href="files/plakat100dpi.png">Plakat konferencji (100 dpi)</a></li>
	<li><a href="files/Piknik_Naukowy_2006.swf">Animacja reklamowa</a></li>
	<li><a href="files/konf2006.avi">Krótki film reklamowy</a></li>
</ul>

<?php
	p_footer();
?>
