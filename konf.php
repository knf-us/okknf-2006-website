<?php
	include_once("header.php");
	include_once("footer.php");
	include_once("reg/common.php");
	p_header("PIKNIK NAUKOWY 2006 -- Informacje naukowe", "konf");
?>

<p>
<span class="header">Informacje naukowe</span>
</p>

<h2>Proponowane sesje wykładowe</h2>
<ul>
	<li><b>Fizyka doświadczalna i stosowana</b>
		<ul>
			<li>Dydaktyka Fizyki</li>
			<li>Zastosowania Fizyki (m.in. fizyka medyczna)</li>
			<li>Fizyka Fazy Skondensowanej</li>
			<li>Fizyka Jądrowa i Jej Zastosowania</li>
		</ul>
	</li>	
	<li><b>Fizyka teoretyczna</b>
		<ul>
			<li>Teoria Ciała Stałego</li>
			<li>Metody Komputerowe Fizyki</li>
			<li>Teoria Pola i Cząstki Elementarne</li>
			<li>Astrofizyka i Kosmologia</li>
			<li>Fizyka Współczesna</li>
		</ul>
	</li>
</ul>

<p>
Przygotowywane wykłady powinny mieć charakter przystępny 
dla ogółu uczestników konferencji. Przewidywany czas na jedno 
wystąpienie to ok. 25 minut, w co wliczone jest od 5 do 10 minut
pytań i dyskusji. Przedłużające się dyskusje będziemy przenosić 
do "kuluarów". 
</p>

<p>
	<span class="header">Rejestracja na konferencję</span>
Na konferencję należy rejestrować się <a href="<?php echo $root_uri; ?>/reg/">on-line</a>.	
</p>

<p>
	<span class="header">Uczestnicy konferencji</span>
</p>

<?php
		$opt_degrees = array(
			"none" => "",
			"inz" => "inż.",
			"mgr" => "mgr",
			"mgr inz" => "mgr inż.",
			"dr" => "dr",
			"dr hab" => "dr hab.",
			"doc" => "doc.",
			"prof" => "prof. dr hab.",
		);
?>

<h2>Przedstawiciele świata nauki</h2>
<?php
	$res = $dbc->query("SELECT acronym, fullname FROM ".TBL_UNIS." ORDER BY acronym");
	$unis = array();
	
	while($r = $dbc->fetchAssoc($res)) {
		$unis[$r["acronym"]] = $r["fullname"];
	}
		
	$res = $dbc->query("SELECT `desc`, name, surname, degree, university FROM ".TBL_USER." WHERE show_web = 1 && ".
						"(degree != 'none' && degree != 'inz' && degree != 'mgr' && degree != 'mgr inz') ".
						"ORDER BY surname, name ASC");
	if ($dbc->numRows($res) == 0) {
		echo '<p>Żadni przedstawiciele świata nauki nie potwierdzili jeszcze swojego przyjazdu na konferencję.</p>';
	} else {
		echo '<ol>';
		while ($r = $dbc->fetchAssoc($res)) {
			$deg = $opt_degrees["".$r["degree"].""];
			echo "<li> $deg $r[name] $r[surname] (";
			if ($unis[$r["university"]] != "") {
				echo "<abbr title=\"".$unis[$r["university"]]."\">$r[university]</abbr>";
			} else {
				echo $r["university"];
			}
			echo ")";
			if ($r["desc"]) {
				echo " -- ".$r["desc"];
			}
			echo "</li>\n";
		}
		echo '</ol>';
	}
?>



<h2>Studenci</h2>
<?php
	$res = $dbc->query("SELECT name, surname, degree, university FROM ".TBL_USER." WHERE show_web = 1 && ".
						"(degree = 'none' || degree = 'inz' || degree = 'mgr' || degree = 'mgr inz') ".
						"ORDER BY surname, name ASC");
	if ($dbc->numRows($res) == 0) {
		echo '<p>Żadni studenci nie potwierdzili jeszcze swojego przyjazu na konferencję.</p>';
	} else {
		echo '<ol>';
		while ($r = $dbc->fetchAssoc($res)) {
			$deg = $opt_degrees["".$r["degree"].""];
			echo "<li> $deg $r[name] $r[surname] (";
			if ($unis[$r["university"]] != "") {
				echo "<abbr title=\"".$unis[$r["university"]]."\">$r[university]</abbr>";
			} else {
				echo $r["university"];
			}
			
			echo ")</li>\n";
		}
		echo '</ol>';
	}
?>

<h2>Skróty</h2>
<ul>
<?php
	foreach ($unis as $abbr => $full) {
		echo "<li><b>$abbr</b> - $full</li>\n";
	}
?>
</ul>

<p>
	<span class="header">Plan konferencji</span>
</p>

<table class="schedule">
<tr><th style="width: 15%;">godz.</th><th>czwartek (20.04)</th><th>piątek (21.04)</th><th>sobota (22.04)</th><th>niedziela (23.04)</th></tr>
<tr><td>08:00 - 08:30</td>
	<td rowspan="11">rejestracja uczestników</td><td rowspan="2">&nbsp;</td><td rowspan="2">&nbsp;</td><td rowspan="2">&nbsp;</td>
</tr>
<tr><td>08:30 - 09:00</td></tr>
<tr><td>09:00 - 09:30</td>	<td rowspan="3">śniadanie</td><td rowspan="3">śniadanie</td><td rowspan="3">śniadanie</td></tr>
<tr><td>09:30 - 10:00</td></tr>
<tr><td>10:00 - 10:30</td></tr>
<tr><td>10:30 - 11:00</td>	
	<td rowspan="6">I sesja wykładowa</td><td rowspan="6">III sesja wykładowa</td><td rowspan="4">V sesja wykładowa</td>
</tr>
<tr><td>11:00 - 11:30</td></tr>
<tr><td>11:30 - 12:00</td></tr>
<tr><td>12:00 - 12:30</td></tr>
<tr><td>12:30 - 13:00</td>		
	<td rowspan="4">uroczyste zakończenie konferencji oraz obiad</td>
</tr>
<tr><td>13:00 - 13:30</td></tr>
<tr><td>13:30 - 14:00</td><td rowspan="3">obiad</td><td rowspan="3">obiad</td><td rowspan="3">obiad</td></tr>
<tr><td>14:00 - 14:30</td></tr>
<tr><td>14:30 - 15:00</td>
	<td rowspan="14">&nbsp;</td>
</tr>
<tr><td>15:00 - 15:30</td><td rowspan="3">czas wolny</td><td rowspan="6">II sesja wykładowa</td><td rowspan="6">IV sesja wykładowa</td></tr>
<tr><td>15:30 - 16:00</td></tr>
<tr><td>16:00 - 16:30</td></tr>
<tr><td>16:30 - 17:00</td><td rowspan="3">wykłady inauguracyjne</td></tr>
<tr><td>17:00 - 17:30</td></tr>
<tr><td>17:30 - 18:00</td></tr>
<tr><td>18:00 - 18:30</td>
<td rowspan="4">powitalna lampka wina - uroczysta kolacja - rozpoczęcie konferencji (sugerowane odpowiednie stroje :))</td>
<td rowspan="2">kolacja</td>
<td rowspan="2">kolacja</td></tr>
<tr><td>18:30 - 19:00</td></tr>
<tr><td>19:00 - 19:30</td>
	<td rowspan="5">impreza integracyjna w klubie studenckim do bladego świtu ;)</td>
	<td rowspan="5">proponowana wycieczka do Czeskiego Cieszyna</td>
</tr>
<tr><td>19:30 - 20:00</td></tr>
<tr><td>20:00 - 20:30</td><td rowspan="3">&nbsp;</td></tr>
<tr><td>20:30 - 21:00</td></tr>
<tr><td class="nowrap">21:00 - +\infty ;)</td></tr>
</table>

<p>W razie konieczności zostanie <b>zmniejszona</b> liczba sesji wykładowych.</p>
<p>W tracie konferencji przewidujemy sesję posterową.</p>


<?php
	p_footer();
?>
