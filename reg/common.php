<?php
	ob_start();

	include("config.php");
	include("db.php");
	include("html.php");
	include("form.php");
	include("session.php");

	srand();

	mb_language('Neutral');
	mb_internal_encoding("UTF-8");
	mb_http_input("UTF-8");
	mb_http_output("UTF-8");

	$dbc = new MySQL_DB;
	$dbc->query("set charset utf8");
	$html = new HTML;
	$ssn = new Session($dbc, $html);
/*
	vim: set ts=4 encoding=utf-8 nowrap:
*/
?>
