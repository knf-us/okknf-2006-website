<?php

class MySQL_DB 
{
	var $dbl;		/* database link */
	var $valid_keys = array('login', 'passwd', 'name', 'surname', 'email', 
							'phone', 'degree', 'address', 'city', 'zip', 
							'food', 'room', 'university', 'comments',
							'lecture', 'lecture_title', 'poster', 'poster_title');

	function MySQL_DB() 
	{
		$this->dbl = mysql_connect(DB_HOST, DB_USER, DB_PASS) or 
					 die('Could not connect: '.mysql_error());

		mysql_select_db(DB_NAME) or 
					 die('Could not select database');
	}
	
	function activateUser($login, $confcode) 
	{
		$login = mysql_real_escape_string($login);
		$confcode = mysql_real_escape_string($confcode);

		$res = mysql_query("UPDATE ".TBL_USER." SET active = '1', confcode = '' ".
					"WHERE login = '$login' AND confcode = '$confcode' AND active = 0");

		if (!$res) {
			die("Failed to activate user: ".mysql_error());
		}

		return (mysql_affected_rows() == 1 ? true : false);
	}

	function deactivateUser($login, $confcode)
	{
		$login = mysql_real_escape_string($login);
		$res = mysql_query("UPDATE ".TBL_USER." SET active = '0', confcode = '$confcode' WHERE ".
						   "login = '$login' AND active = '1'");

		if (!$res) {
			die("Failed to deactivate: ".mysql_error());
		}

		return (mysql_affected_rows() == 1 ? true : false);
	}

	function loginTaken($login) 
	{
		$login = mysql_real_escape_string($login);
		$res = mysql_query("SELECT login FROM ".TBL_USER." WHERE login = '$login'");

		if (!$res) {
			die("Failed to check login: ".mysql_error());
		}

		return (mysql_num_rows($res) > 0 ? true : false);
	}

	function emailTaken($email)
	{
		$email = mysql_real_escape_string($email);
		$res = mysql_query("SELECT login FROM ".TBL_USER." WHERE email = '$email'");

		if (!$res) {
			die("Failed to check e-mail: ".mysql_error());
		}

		return (mysql_num_rows($res) > 0 ? true : false);
	}

	function checkPasswd($login, $passwd) 
	{
		$passwd = sha1($passwd);
		$login = mysql_real_escape_string($login);
		$res = mysql_query("SELECT login FROM ".TBL_USER." WHERE login = '$login' AND passwd = '$passwd' AND active = '1'");

		if (!$res) {
			die("Failed to check passwd: ".mysql_error());
		}

		return (mysql_num_rows($res) == 1 ? true : false);
	}

	function setPasswd($login, $passwd)
	{	
		$passwd = sha1($passwd);
		$login = mysql_real_escape_string($login);
		$res = mysql_query("UPDATE ".TBL_USER." SET passwd = '$passwd' WHERE login = '$login' AND active = '1'");

		return (mysql_errno() == 0 ? true : false);
	}

	function checkSession($login, $uid, $sid, $ip)
	{
		$login = mysql_real_escape_string($login);
		$uid = mysql_real_escape_string($uid);
		$sid = mysql_real_escape_string($sid);
		$ip = mysql_real_escape_string($ip);
		$res = mysql_query("SELECT login FROM ".TBL_USER." WHERE login = '$login' AND ".
						   "uid = '$uid' AND session = '$sid' AND ip = '$ip' AND active = '1'");

		if (!$res) {
			die("Failed to check session: ".mysql_error());
		}

		return (mysql_num_rows($res) == 1 ? true : false);
	}

	function setFields($login, $fields)
	{
		$qry = "";
		$login = mysql_real_escape_string($login);

		foreach ($fields as $key => $value) {
			$qry .= "$key = '".mysql_real_escape_string($value)."', ";	
		}
		
		$qry = substr($qry, 0, -2);
		$res = mysql_query("UPDATE ".TBL_USER." SET $qry WHERE login = '$login' AND active = '1'");

		if (!$res) {
			die("Failed to set fields: ".mysql_error());
		}

		return (mysql_affected_rows() == 1 ? true : false);
	}

	function getFields($login, $fields, $type = "array")
	{
		$q = implode(", ", $fields);
		$res = mysql_query("SELECT $q FROM ".TBL_USER." WHERE login = '$login'");

		if (!$res) {
			die("Failed to get fields: ".mysql_error());
		}
		
		if ($type == "array") {
			return mysql_fetch_row($res);
		} else {
			return mysql_fetch_assoc($res);
		}
	}

	function getGroups($login) 
	{
		$groups = $this->getFields($login, array("groups"));
		return explode(',', $groups[0]);
	}

	function query($query)
	{
		return mysql_query($query);
	}

	function fetchRow($res)
	{
		return mysql_fetch_row($res);
	}

	function fetchAssoc($res)
	{
		return mysql_fetch_assoc($res);
	}

	function numRows($res)
	{
		return mysql_num_rows($res);
	}
		
	function escape($val)
	{	
		return mysql_real_escape_string($val);
	}	

	function addUser($hash, $confcode) 
	{
		foreach ($this->valid_keys as $key) {
			if ($hash["$key"] != "") { 
				${$key} = mysql_real_escape_string($hash["$key"]);
			} else {
				${$key} = "";
			}
		}

		$passwd = sha1($passwd);
		$res = mysql_query("INSERT INTO ".TBL_USER."(login,passwd,name,surname,email,phone,".
						   "active,paid,confcode,regtime,address,city,zip,degree,food,room,university,comments,".
						   "lecture,lecture_title,poster,poster_title) ".
						   "VALUES('$login','$passwd', '$name', '$surname', '$email', ".
						   "'$phone', 0, 0, '$confcode', NOW(), '$address', '$city', '$zip', ".
				   		   "'$degree', '$food', '$room', '$university', '$comments', ".
						   "'$lecture', '$lecture_title', '$poster', '$poster_title')");
		if (!$res) {
			die("Failed to add a new user: ".mysql_error());
		}

		return true;
	}

	function minutesToSQL($time)
	{
		return sprintf("%02d%02d00", floor($time/60), $time % 60);
	}

	function addEvent($hash) {
	
		foreach ($hash as $key => $value) {
			${$key} = mysql_real_escape_string($value);
		}	

		$start = $this->minutesToSQL($start);
		$end   = $this->minutesToSQL($end);

		/* Check for collisions with other entries in the DB */
		$res = mysql_query("SELECT * FROM ".TBL_SCHED." WHERE login = '$login' && ".
						   "day = '$day' && ((start > '$start' && end < '$end') || ".
						   "(start < '$start' && end > '$start') || ".
						   "(start < '$end' && end > '$end'))");

		if (mysql_num_rows($res) > 0) {
			return false;
		}

		$res = mysql_query("INSERT INTO ".TBL_SCHED." (login, day, start, end, pri, descpt) ".
						   "VALUES('$login','$day','$start','$end','$pri','$descpt')");

		if (!$res) {
			die("Failed to add a new event: ".mysql_error());
		}

		return true;
	}

	function delEvents($login, $day, $start, $end) {
		$login = mysql_real_escape_string($login);
		$day = mysql_real_escape_string($day);

		$start = $this->minutesToSQL($start);
		$end   = $this->minutesToSQL($end);

		$res = mysql_query("DELETE FROM ".TBL_SCHED." WHERE login = '$login' AND day = '$day' AND ".
							"start >= '$start' && start <= '$start'");

		if (!$res) {
			die("Failed to delete events: ".mysql_error());
		}

		return (mysql_affected_rows() == 1 ? true : false);
	}

	function getValidKeys()
	{
		return $this->valid_keys;
	}
}

/*
	vim: set ts=4 encoding=utf-8 nowrap:
*/
?>
