<?php

class Form
{
	var $errors = array(); 
	var $known_fields = array();
	var $fields = array();
	var $html;
	var $dbc;

	function Form(&$_html, &$_dbc)
	{
		$this->html =& $_html;
		$this->dbc  =& $_dbc;
	}

	function addError($err)
	{
		array_push($this->errors, $err);
	}

	function setField($field, $val)
	{
		if (in_array($field, $this->known_fields)) {
			$this->fields["$field"] = $val;
		}
	}

	function setFields($fields)
	{
		foreach ($fields as $key => $val) {
			if (in_array($key, $this->known_fields)) {
				$this->fields["$key"] = $val;
			}
		}		
	}

	function printErrors()
	{
		if ($this->numErrors() > 0) {
			$s = implode("<br />", $this->errors);
			$this->html->errorMsg($s);
		}
	}

	function printOpts($opts, $selected) {
		foreach ($opts as $id => $desc) {
			echo '<option value="'.$id.'"';
			if ($id == $selected) {
				echo ' selected';
			}	
			echo '>'.$desc.'</option>'."\n";
		}
	}

	function printCheckbox($name, $value) {
		echo '<input type="checkbox" name="'.$name.'"';
		if ($value == "on" || $value == 1) {
			echo ' checked';
		}
		echo ' />';
	}

	function printEdit($name, $value, $len = -1) {
		echo '<input type="edit" name="'.$name.'" value="'.$value.'"';
		if ($len > 0) {
			echo ' size="'.$len.'"';
		}
		echo ' />';
	}
	
	function printSelect($name, $value, $opts) {
		echo '<select name="'.$name.'">';
		$this->printOpts($opts, $value);
		echo '</select>';
	}
	
	function validateOpts($opts, $field) {
		foreach ($opts as $id => $desc) {
			if ($id == $field) {
				return true;
			}
		}
		return false;
	}

	function printRow($obj) 
	{
		echo '<tr><td class="'.($obj->getReq() ? 'desc' : 'descopt').'">'.$obj->getDesc();
		echo '</td><td class="inpt">'.$obj->html.'</td></tr>'."\n";
	}
	
	function numErrors()
	{
		return count($this->errors);
	}
}

/* Form element class */
class Fel
{
	var $name, $required, $desc, $val, $form;
	
	function Fel($_form, $_name, $_required, $_desc)
	{
		$this->form = $_form;
		$this->name = $_name;
		$this->required = $_required;
		$this->desc = $_desc;
	}

	function validate($val)
	{
		$this->val = $val;
			
		if ($this->required && $val == "") {
			return false;
		} else {
			return true;
		}
	}

	function getDesc()	{	return $this->desc;			}
	function getReq()	{	return $this->required;		}
}

class FelText extends Fel
{
	var $len;

	function FelText($_form, $_name, $_req, $_desc, $_len = -1)
	{
		parent::Fel($_form, $_name, $_req, $_desc);
		$this->len = $_len;
	}
	
	function html()
	{
		$this->form->printEdit($this->name, $this->val, $this->len);
	}
}

class FelPasswd extends Fel
{

}

class FelEmail extends Fel
{

}

class FelBool extends Fel
{

}

class FelLongtext extends Fel
{

}

class FelSelect extends Fel
{
	var $options;
	
	function FelSelect($_name, $_required, $_desc, $_options)
	{
		$this->name = $_name;
		$this->required = $_required;
		$this->desc = $_desc;
		$this->options = $_options;
	}
}

class RegForm extends Form
{	
	var $known_fields = array(
			"login", "passwd", "passwd2", "name", "surname", "email",
			"address", "phone", "city", "zip", "degree", "university",
			"food", "room", "comments", "lecture", "lecture_title",
			"poster", "poster_title"
		);
/*
		
	function initFels()
	{
		new FelText('login',	true, 'Login');
		new FelPasswd('passwd',	true, 'Hasło');
		new FelPasswd('passwd2',true, 'Hasło (potwierdzenie)');
		new FelText('name',		true, 'Imię');
		new FelText('surname',	true, 'Nazwisko');
		new FelEmail('email',	true, 'E-mail');
		new FelText('address',	true, 'Adres');
		new FelText('phone',	false, 'Telefon kontakotwy');
		new FelText('city',		true, 'Miasto');
		new FelText('zip',		true, 'Kod pocztowy');
		new FelSelect('degree', true, 'Tytuł', $opt_degrees);
		new FelText('university',		true, 'Uczelnia');
		new FelSelect('food',			true, 'Wyżywienie', $opt_food);
		new FelSelect('room',			true, 'Pokój', $opt_room);
		new FelLongtext('comments',		false, 'Uwagi');
		new FelBool('lecture',			false, 'Jestem zainteresowana/y wygłoszeniem wykładu');
		new FelText('lecture_title',	false, 'Tytuł'); 
		new FelBool('poster',			false, 'Jestem zainteresowana/y przedstawieniem plakatu');
		new FelText('poster_title',		false, 'Tytuł');
	}
*/	
		
	var $opt_degrees = array(
			"none" => "(student)",
			"inz" => "inż.",
			"mgr" => "mgr",
			"mgr inz" => "mgr inż.",
			"dr" => "dr",
			"dr hab" => "dr hab.",
			"doc" => "doc.",
			"prof" => "prof.",
		);

	var $opt_food = array(
			"std" => "standardowe",
			"veg" => "wegetariańskie",
		);

	var	$opt_room = array(
			"nopref" => "brak preferencji",
			"single" => "1-osobowy",
			"double" => "2-osobowy",
		);

	var $mode;

	function RegForm(&$_html, &$_dbc, $_mode = "register")
	{
		$this->html =& $_html;
		$this->dbc  =& $_dbc;
		$this->mode = $_mode;
	}

	function printForm() 
	{
		if ($this->mode == "edit") {
			echo '<p><span class="header">edycja danych</span></p>';
		} else {
			echo '<p><span class="header">rejestracja nowego użytkownika</span></p>';
		}
		$this->printErrors();

		/* Import data into local variables so that it can be
		 * easily used in the here-document below. */
		foreach ($this->known_fields as $key) {
			if ($this->fields["$key"] != "") {
				${$key} = $this->fields["$key"];
			} else {
				${$key} = "";
			}	
		}

		if ($gg == 0) {
			$gg = "";
		}

		if ($icq == 0) {
			$icq = "";
		}

		echo '<form action="user.php" method="post">';
		
		if ($this->mode == "register") {
			echo '<input type="hidden" name="cmd" value="register">';
		} else {
			echo '<input type="hidden" name="cmd" value="edit">';
		}
		echo '<table class="tform">';
		if ($this->mode == "register") { 
			echo <<<HTML
<tr><td class="desc" style="width: 30%">Login:</td><td class="inpt" style="width: 70%"><input type="text" name="login" value="$login" /></td></tr>
<tr><td class="desc">Hasło:</td><td class="inpt"><input type="password" name="passwd" /></td></tr>
<tr><td class="desc">Hasło (potwierdzenie):</td><td class="inpt"><input type="password" name="passwd2" /></td></tr>
<tr><td class="desc">Tytuł: </td><td class="inpt"><select name="degree">
HTML;
			$this->printOpts($this->opt_degrees, $degree);
			echo <<<HTML
</select></td></tr>
<tr><td class="desc">Imię:</td><td class="inpt"><input type="text" name="name" value="$name" /></td></tr>
<tr><td class="desc">Nazwisko:</td><td class="inpt"><input type="text" name="surname" value="$surname" /></td></tr>
<tr><td class="desc">E-mail:</td><td class="inpt"><input type="text" name="email" value="$email" /></td></tr>
<tr><td class="desc">Miasto:</td><td class="inpt"><input type="text" name="city" value="$city" /></td></tr>
<tr><td class="desc">Kod pocztowy:</td><td class="inpt"><input type="text" name="zip" value="$zip" /></td></tr>
<tr><td class="desc">Adres:</td><td class="inpt"><input type="text" name="address" value="$address" size="40" /></td></tr>
<tr><td class="desc">Uczelnia:</td><td class="inpt"><input type="text" name="university" valie="$university" size="40" /></td></tr>
HTML;
		}
		echo <<<HTML
<tr><td class="desc">Wyżywienie:</td><td class="inpt"><select name="food">
HTML;
		$this->printOpts($this->opt_food, $food);
		echo <<<HTML
</select></td></tr>
<tr><td class="desc">Pokój:</td><td class="inpt"><select name="room">
HTML;
		$this->printOpts($this->opt_room, $room);
		echo <<<HTML
</select> (*)</td></tr>

<tr><td class="descopt">Telefon kontaktowy:</td><td class="inpt"><input type="text" name="phone" value="$phone" /></td></tr>
<tr><td class="descopt">Uwagi:</td><td class="inpt"><textarea name="comments" rows="4" cols="65">$comment</textarea></td></tr>
<tr>
	<td class="descopt">Jestem zainteresowana/y wygłoszeniem wykładu:</td>
	<td class="inpt">
HTML;
		$this->printCheckbox("lecture", $lecture);
		echo <<<HTML
		Tytuł: <input type="text" name="lecture_title" size="75" value="$lecture_title" />
	</td>
</tr>
<tr>
	<td class="descopt">Jestem zainteresowana/y przedstawieniem plakatu:</td>
	<td class="inpt">
HTML;
		$this->printCheckbox("poster", $poster);		
		echo <<<HTML
		Tytuł: <input type="text" name="poster_title" size="75" value="$poster_title" />	
	</td>
</tr>
HTML;

		if ($this->mode == "register") {
			echo <<<HTML
<tr><td colspan="2" style="padding-top: 2em; padding-bottom: 2em;">
Wysłanie powyższego formularza jest równoznaczne z wyrażeniem zgody na przetwarzanie 
podanych w nim danych osobowych dla potrzeb niezbędnych do organizacji Konferencji
(zgodnie z ustawą o ochronie danych osobowych z dn. 29.08.97 Dz. U. 133 Poz. 883).</td></tr>
HTML;
		}

		echo <<<HTML
<tfoot><tr><td colspan="2"><input type="submit" value="Wyślij" /></td></tr></tfoot>
</table>
</form>

<p>Pola opisane pogrubioną czcionką muszą zostać wypełnione.</p>

<p>(*):<br />
W przypadku braku miejsc w pokojach jednoosobowych zostaną przydzielone miejsca w pokojach dwuosobowych.<br /><br />
Istnieje mozliwość zarezerwowania 6-osobowego pokoju dla większej, zoorganizowanej grupy. W sprawie rezerwacji
prosimy kontaktować się bezpośrednio z organizatorami.
</p>

HTML;
	}

	function validate(&$input, $login = "") 
	{
		$this->setFields($input);

		/* Remove unnecessary spaces from all fields except the ones that
		 * contain passwords. */
		foreach ($this->known_fields as $key) {
			if ($key == "passwd" || $key == "passwd2") {
				continue;
			}

			if (defined($input["$key"])) {
				$input["$key"] = trim($input["$key"]);
			}
		}

		/* First letter for name, surname and city should be uppercase */
		$input["name"]    = ucfirst($input["name"]);
		$input["surname"] = ucfirst($input["surname"]);
		$input["city"]    = ucfirst($input["city"]);

		if ($input["lecture"] == "on") {
			$input["lecture"] = 1;
		} else {
			$input["lecture"] = 0;
		}

		if ($input["poster"] == "on") {
			$input["poster"] = 1;
		} else {
			$input["poster"] = 0;	
		}

		/* Registration of a new user */	
		if ($this->mode == "register") {
			if (strlen($input["login"]) < 3) {
				$this->addError("Login musi składać się przynajmniej z 3 znaków.");
			}
			if ($input["name"] == "") {
				$this->addError("Musisz podać swoje imię.");
			}
			if ($input["surname"] == "") {
				$this->addError("Musisz podać swoje nazwisko.");
			}
			if ($input["university"] == "") {
				$this->addError("Musisz podać nazwę swojej uczelni.");
			}
			if ($input["address"] == "") {
				$this->addError("Musisz podać swój adres.");
			}
			if ($input["city"] == "") {
				$this->addError("Musisz podać miasto, w którym mieszkasz.");
			}
			if ($input["zip"] == "") {
				$this->addError("Musisz podać kod pocztowy.");
			}
			if (!$this->validateOpts($this->opt_degrees, $input["degree"])) {
				$this->addError("Musisz podać swój tytuł naukowy.");
			}
			if (!preg_match('/[a-zA-Z0-9_\-\.]+@([a-zA-Z0-9_\-\.]+\.)+[a-zA-Z]{2,3}/', $input["email"])) {
				$this->addError("Musisz podać prawidłowy adres e-mail.");
			}
			if ($this->dbc->loginTaken($input["login"])) {
				$this->addError("Wybrany login jest już zajęty.");
			}
			if ($this->dbc->emailTaken($input["email"])) {
				$this->addError("W naszej bazie danych istnieje już użytkownik korzystający z podanego adresu e-mail.");
			}
			if (strlen($input["passwd"]) < 6) {
				$this->addError("Hasło musi mieć co najmniej 6 znaków.");
			}
			if ($input["passwd"] != $input["passwd2"]) {
				$this->addError("Podane hasła są różne.");
			}
		}
			
		if (!$this->validateOpts($this->opt_food, $input["food"])) {
			$this->addError("Musisz określić swoje preferencje żywieniowe.");
		}
		if (!$this->validateOpts($this->opt_room, $input["room"])) {
			$this->addError("Musisz określić swoje preferencje odnośnie zakwaterowania.");
		}
		
		return ($this->numErrors() > 0 ? false : true);
	}
}

class LoginForm extends Form
{
	function printForm() 
	{
		$this->printErrors();

echo <<<HTML
<form action="user.php" method="post">
<input type="hidden" name="cmd" value="login">
<p><span class="header">Logowanie do systemu</span></p>
<table class="tform">
<tr><td class="desc">Login:</td><td class="inpt"><input type="text" name="login"></td></tr>
<tr><td class="desc">Hasło:</td><td class="inpt"><input type="password" name="passwd"></td></tr>
<tfoot><tr><td colspan="2"><input type="submit" value="Zaloguj"></td></tr></tfoot>
</table>
</form>
HTML;
	}

	function validate(&$input)
	{
		if ($this->dbc->checkPasswd($input["login"], $input["passwd"])) {
			return true;
		} else {
			$this->addError("Nieprawidłowa nazwa użytkownika lub hasło.");
			return false;
		}
	}
}

/*
	vim: set ts=4 encoding=utf-8 nowrap:
*/
?>
