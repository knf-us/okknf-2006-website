<?php

if (file_exists("../header.php")) {
	include_once("../header.php");
	include_once("../footer.php");
}

class HTML
{
	function stdHeader($title) 
	{
		$root_uri = ROOT_URI;	
		p_header("PIKNIK NAUKOWY 2006 -- Strona główna", "reg");
		return;
	}

	function stdFooter()
	{
		p_footer();
		return;
	}

	function errorMsg($msg)
	{
		echo '<p class="error">'.$msg.'</p>'."\n";
	} 

	function infoMsg($msg)
	{
		echo '<p class="info">'.$msg.'</p>'."\n";
	}

	function infoLink($to, $desc)
	{
		echo '<p style="text-align: center;"><a href="'.$to.'">'.$desc.'</a></p>'."\n";
	}

	function sectHeader($text)
	{
		echo '<p><span class="header">'.$text.'</span></p>';
	}
}

/*
	vim: set ts=4 encoding=utf-8 nowrap:
*/
?>
