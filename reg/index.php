<?php
	include_once("common.php");
		
	$html->stdHeader("Strona główna");

	if (!$ssn->loggedIn()) {
		$lf = new LoginForm($html, $dbc);
		$rf = new RegForm($html, $dbc);
	
		$lf->printForm();
		$rf->printForm();
	} else {

		list($name, $surname, $paid) = $dbc->getFields($ssn->getLogin(), array("name","surname","paid"));
		
		echo '<p><span class="header">Twoje konto</span></p>';
		echo "<p>Witaj! Jesteś zalogowany jako <b>$name $surname (".$ssn->getLogin().")</b>.</p>";

		if ($paid) {
			echo '<p>Twoja opłata rejestracyjna została już przyjęta. Dziękujemy.</p>';
		} else {
			echo '<p>Twoja opłata rejestracyjna jeszcze nie wpłynęła na nasze konto.</p>';
		}

		$rf = new RegForm($html, $dbc, "edit");
		$rf->printForm();
		
		echo '<p><a href="'.ROOT_URI.'/user.php?cmd=logout">wyloguj się</a></p>';
	}


	$html->stdFooter();		

/* vim: set ts=4 encoding=utf-8 nowrap: */
?>
