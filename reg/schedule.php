<?php
	include_once("common.php");
	
	$html->stdHeader("Plan tygodnia");
	$days = array("poniedziałek", "wtorek", "środa", "czwartek", "piątek", "sobota", "niedziela");

	if (!$ssn->loggedIn()) {
		$html->infoLink(ROOT_URI.'/index.php', "Przejście do strony logowania");
		return;
	}

	switch ($_REQUEST["cmd"]) {

	case "edit":
		cmd_edit();
		break;
	
	case "list":
		cmd_list();
		break;

	case "cmp":
		cmd_cmp();
		break;
	}


	$html->infoLink(ROOT_URI.'/index.php', "Powrót do strony głównej");
?>

<script type="text/javascript">

var days = new Array(7);

for (i = 0; i < 7; i++) {
	days[i] = new Array(2);
	days[i][0] = -1;
	days[i][1] = -1;
}

function checkRange(day, start, end)
{
	var i;
	var name = 't_' + day + '_row';

	for (i = start; i <= end; i++) {
		var ob = document.getElementById(name + i);
		if (!ob) {
			return false;	
		}
					
		var h = document.getElementById(name + i).className;
		
		if (h != "entry" && h != "entry_sel") {
			return false;
		}
	}
	return true;
}

function toggle(day, row, toggle)
{
	var cbox_name = 'day' + day + '_';
	var tbl_name = 't_' + day + '_row';
	var cbox_ob = document.getElementById(cbox_name+row);
	var fields = <?php echo floor((TIME_END - TIME_START)/TIME_RES) ?>;

	if (toggle) {
		cbox_ob.checked = ! cbox_ob.checked;
	}

	if (days[day][0] == -1) {
		days[day][0] = row;
		setClass(day, row, row, "entry_sel");
	} else if (days[day][1] == -1) {

		if (days[day][0] == row) {
			setClass(day, row, row, "entry");
			days[day][0] = -1;
		} else {
			days[day][1] = row;

			var good = false;

			/* Fix it up if necessary. [0] is always <= [1] */
			if (days[day][0] > days[day][1]) {
				if (checkRange(day, days[day][1], days[day][0])) {
					var t = days[day][1];
					days[day][1] = days[day][0];
					days[day][0] = t;
					good = true;
				} 
			} else if (checkRange(day, days[day][0], days[day][1])) {
				good = true;
			}
		
			if (good) {
				setClass(day, days[day][0], days[day][1], "entry_sel");
			} else {
				document.getElementById(cbox_name + days[day][0]).checked = false;
				setClass(day, days[day][0], days[day][0], "entry");
				setClass(day, row, row, "entry_sel");
				days[day][0] = row;
				days[day][1] = -1;
			}
		}

	/* Extend it down */
	} else if (row < days[day][0]) {
		document.getElementById(cbox_name + days[day][0]).checked = false;
		
		if (checkRange(day, row, days[day][0])) {
			setClass(day, row, days[day][0], "entry_sel");
		} else {
			document.getElementById(cbox_name + days[day][1]).checked = false;
			setClass(day, days[day][0], days[day][1], "entry");
			setClass(day, row, row, "entry_sel");
			days[day][1] = -1;
		}
		
		days[day][0] = row;

	/* Extend it up */
	} else if (row > days[day][1]) {
		document.getElementById(cbox_name + days[day][1]).checked = false;
		if (checkRange(day, days[day][1], row)) {
			setClass(day, days[day][1], row, "entry_sel");
			days[day][1] = row;
		} else {
			document.getElementById(cbox_name + days[day][0]).checked = false;
			setClass(day, days[day][0], days[day][1], "entry");
			setClass(day, row, row, "entry_sel");
			days[day][1] = -1;
			days[day][0] = row;
		}

	/* Shorten it */	
	} else if (row > days[day][0] && row < days[day][1]) {
	
		if (row < days[day][0] + (days[day][1] - days[day][0])/2) {
			document.getElementById(cbox_name + days[day][0]).checked = false;
			setClass(day, days[day][0], row - 1, "entry");
			days[day][0] = row;
		} else {	
			document.getElementById(cbox_name + days[day][1]).checked = false;
			setClass(day, row + 1, days[day][1], "entry");
			days[day][1] = row;
		}

	/* FIXME: This never happens? */
	} else if (row == days[day][0] && row == days[day][1]) {
	
		days[day][0] = days[day][1] = -1;
		setClass(day, row, row, "entry");

	} else if (row == days[day][0]) {
		
		document.getElementById(cbox_name + days[day][0]).checked = false;
		days[day][0]++;
		document.getElementById(cbox_name + days[day][0]).checked = true;
		if (days[day][0] == days[day][1]) {
			days[day][1] = -1;
		}
		setClass(day, row, row, "entry");

	} else if (row == days[day][1]) {
			
		document.getElementById(cbox_name + days[day][1]).checked = false;
		days[day][1]--;
		document.getElementById(cbox_name + days[day][1]).checked = true;
		if (days[day][0] == days[day][1]) {
			days[day][1] = -1;
		}
		setClass(day, row, row, "entry");
	}

	/* Clear entries for the other days. We can only modify a single
	 * event on a single day at once. */
	for (i = 0; i < 7 ; i++) {
		var cbox_t = 'day' + i + '_';
		
		if (i == day || (days[i][0] < 0 && days[i][1] < 0)) {
			continue;
		}

		if (days[i][0] >= 0 && days[i][1] >= 0) {
			document.getElementById(cbox_t + days[i][0]).checked = false;
			document.getElementById(cbox_t + days[i][1]).checked = false;
			setClass(i, days[i][0], days[i][1], "entry");
			days[i][0] = days[i][1] = -1;
		} else if (days[i][0] >= 0) {
			document.getElementById(cbox_t + days[i][0]).checked = false;
			setClass(i, days[i][0], days[i][0], "entry");
			days[i][0] = -1;
		} else {
			document.getElementById(cbox_t + days[i][1]).checked = false;
			setClass(i, days[i][1], days[i][1], "entry");
			days[i][1] = -1;
		}
	}
}

function setClass(day, from, to, cls) 
{
	var tbl_name = 't_' + day + '_row';
	var i;

	for (i = from; i <= to; i++) {
		document.getElementById(tbl_name + i).className = cls;
	}
}

function descHlght(login, from, to, status)
{
	var i;

	for (i = from; i <= to; i++) {
		var obj = document.getElementById('desc' + login + '_' + i);
		if (status) {
			obj.className = 'desc-hili';
		} else {
			obj.className = 'desc';
		}
	}
}

</script>

<?php

function edit_del()
{
	global $html, $ssn, $dbc;

	$day = -1;
	$start = 0;

	foreach ($_REQUEST as $key => $val) {
		if (preg_match("/day(\d+)_(\d+)/", $key, $mt)) {
			$day = $mt[1];
			$start = TIME_START + $mt[2] * TIME_RES;
			$dbc->delEvents($ssn->getLogin(), $day, $start, $start + TIME_RES);
		}
	}
}

function edit_add()
{
	global $html, $ssn, $dbc;

	$day = -1;
	$start = 0;
	$end = 0;	

	foreach ($_REQUEST as $key => $val) {
		if (preg_match("/day(\d+)_(\d+)/", $key, $mt)) {
			if ($day == -1) {
				$day = $mt[1];
				$start = TIME_START + $mt[2] * TIME_RES;
			} else if ($day != $mt[1] || $end > 0) {
				$html->errorMsg("Możesz zaznaczyć tylko dwa pola w pojedynczej kolumnie.");
				return;
			} else {
				$end = TIME_START + ($mt[2]+1) * TIME_RES;
			}
		}
	}

	/* Invalid request, no day and start/end time selected. */
	if ($day < 0) {
		return;
	}

	/* If no second field was selected, the event lasts a single
	 * timeslot. */
	if ($end == 0) {
		$end = $start + TIME_RES;
	/* BUG, should never happen */
	} else if ($end < $start) {
		return; 
	}

	$pri = $_REQUEST["pri"];
	if ($pri < -4 || $pri > 4) {
		$pri = 0;
	}

	$req = array( 
				"login" => $ssn->getLogin(),
				"pri" => $pri,
				"start" => $start,
				"end" => $end,
				"day" => $day,
				"descpt" => $_REQUEST["descpt"]
			);
	if (!$dbc->addEvent($req)) {
		$html->errorMsg("Dodanie nowej pozycji nie powiodło się -- wstąpiła kolizja ".
					    "z inną pozycją istniejącą już w bazie danych");
	}
}

function cmd_edit()
{	
	global $html, $ssn, $dbc;

	if ($_REQUEST["subm_add"] != "") {
		edit_add();			
	} else if ($_REQUEST["subm_del"] != "") {
		edit_del();
	}
	show_schedule($ssn->getLogin(), true);
}

/* A simple class representing a single event */
class Event
{
	var $start, $end, $pri, $desc;
	var $done = false;	

	function Event($_start, $_end, $_pri, $_desc) {
		$this->start = $_start;
		$this->end   = $_end;
		$this->pri	 = $_pri;
		$this->desc  = $_desc;
	}
}

function show_schedule($login, $editmode)
{
	global $days, $dbc;

	$events = array();
	$rowspans = array();

	for ($i = 0; $i < 7; $i++) {
		$events[$i] = array();
		$rowspans[$i] = 0;
	}

	/* Fetch event data from the database */
	$res = $dbc->query("SELECT day, HOUR(start) * 60 + MINUTE(start) AS start, HOUR(end) * 60 + MINUTE(end) AS end, ".
						"pri, descpt FROM ".TBL_SCHED." WHERE login = '$login' ORDER BY start ASC");
	
	while(list($day, $start, $end, $pri, $desc) = $dbc->fetchRow($res)) {
		array_push($events[$day], new Event($start, $end, $pri, $desc));
	}

	if ($editmode) {
		echo '<form action="schedule.php" method="post">'."\n";
		echo '<input type="hidden" name="cmd" value="edit">'."\n";
	}

	echo '<table class="schedule">'."\n";
	echo '<colgroup style="width: 9%"><col /></colgroup>'."\n";

	if ($editmode) {
		for ($i = 0; $i < 7; $i++) {
			echo '<colgroup span="2"><col class="cbox" /><col class="desc" /></colgroup>'."\n";
		}
	} else {
		echo '<colgroup span="7"><col class="desc" /><col class="desc" /><col class="desc" />';
		echo '<col class="desc" /><col class="desc" /><col class="desc" /><col class="desc" />';
		echo "</colgroup>\n";
	}

	echo "<tr><th>czas</th>\n";

	if ($editmode) {
		$span = 'colspan="2"';
	} else {
		$span = "";
	}
	/* Print day names for the table header */
	foreach ($days as $day) {
		echo "\t<th $span>$day</th>\n";
	}
	echo "</tr>\n";

	for ($time = TIME_START, $row = 0; $time < TIME_END; $time += TIME_RES, $row++) {

		/* Timeslot description -- the leftmost column.
		 * Each cell has the ID: desc<login>_<rownum> */
		echo '<tr>'."\n\t".'<td class="desc" id="desc'."${login}_$row".'" nowrap>';
		echo time_human($time)." - ".time_human($time + TIME_RES);
		echo "</td>\n";

		for ($i = 0; $i < count($days); $i++) {
			$el = "day${i}_${row}";

			/* Are we already inside an event in this column? */
			if ($rowspans[$i] <= 0) {

				/* Match an event? */	
				if (count($events[$i]) > 0 && $time >= $events[$i][0]->start) {
					/* Remove the event from the queue */
					$obj = array_shift($events[$i]);

					/* Calculate rowspan */
					$rowspan = (($obj->end - $obj->start) + ($obj->start - $time)) / TIME_RES;
					$rowspans[$i] = $rowspan - 1;

					/* JavaScript functions to hilight the timeslot descriptions
					 * when the mouse cursor is placed over an event */
					$js = 'onmouseover="descHlght('."'$login',$row,".($row+$rowspan-1).',true)" '.
						  ' onmouseout="descHlght('."'$login',$row,".($row+$rowspan-1).',false)"';

					echo "\t";
					if ($editmode) {
						echo '<td rowspan="'.$rowspan.'" class="pri'.($obj->pri + 4).'" '.$js.'>';
						echo '<input type="checkbox" name="'.$el.'" id="'.$el.'"/></td>';
					}
					echo '<td rowspan="'.$rowspan.'" class="pri'.($obj->pri + 4).'" '.$js.'>';
					echo $obj->desc."</td>\n";
				} else {
					$js = "toggle($i, $row, true);";
					$js_nc = "toggle($i, $row, false);";

					$hl = 'onmouseover="descHlght('."'$login',$row,$row".',true)" '.
						  ' onmouseout="descHlght('."'$login',$row,$row".',false)"';
										
					echo "\t";
					if ($editmode) {
						echo '<td class="cbox" '.$hl.'><input type="checkbox" onclick="'.$js_nc.'" name="'.$el.'" id="'.$el.'"/></td>';
						echo '<td class="entry" id="t_'.$i.'_row'.$row.'" onclick="'.$js.'" '.$hl.'>&nbsp;</td>'."\n";
					} else {
						echo '<td class="entry" '.$hl.'>&nbsp;</td>'."\n";
					}
				}
			} else {
				$rowspans[$i]--;
			}
		}

		echo "</tr>\n";
	}
	
	echo '</table>';

	/* We only need to display the instructions if the schedule is
	 * displayed in edit mode. */
	if ($editmode) {
		echo <<<HTML
<ul>
<li>Dodawanie nowej pozycji
<p>Aby dodać nową pozycję, zaznacz jej początek oraz koniec w powyższej tabeli
oraz określ swoje preferencje korzystając z poniższego pola. Możesz także dodać
słowny opis.</p>

Preferencje: <select name="pri">
<option value="4">bardzo chętnie przyjdę</option>
<option value="3">chętnie przyjdę</option>
<option value="2">pasuje mi</option>
<option value="1">może być</option>
<option value="0" selected>mam wolne, ale cenię sobie wolny czas</option>
<option value="-1">zajęte, ale mogę łatwo dostosować</option>
<option value="-2">zajęte, ale mogę z trudem dostosować</option>
<option value="-3">zajęte, ale mogę z wielkim trudem dostosować</option>
<option value="-4">zajęte i nie mogę dostosować</option>
</select><br />

Opis: <input name="descpt" type="text" size="30" /><br />
<input type="submit" name="subm_add" value="Dodaj pozycję" /><br /><br />
</li>

<li>Usuwanie pozycji
<p>Aby usunąć pozycję, zaznacz ją w tabeli powyżej a następnie kliknij przycisk "Usuń pozycję".<br />
<input type="submit" name="subm_del" value="Usuń pozycję" /></p>
</li>

</ul>
</form>

HTML;
	}	
}

function chk_grps()
{
	global $html, $ssn;

	$grp = $_REQUEST["grp"];
	if ($grp == "") {
//		$html->errorMsg("Nieprawidłowa grupa.");
//		$html->infoLink(ROOT_URI.'/index.php', "Przejście do strony głównej");
		return array();
	}
	/* Check whether the user has the privileges necessary to access
	 * the requested groups. */
	$grps = explode(",",$grp);
	if (!$ssn->checkGroups($grps)) {
		return array();
	}

	return $grps;
}

function cmd_list()
{
	global $ssn, $dbc;	

	$grps = chk_grps();	
	if (count($grps) == 0) {
		return;
	}
	$qry = "0";
	foreach ($grps as $t) {
		$t = $dbc->escape($t);
		$qry .= " OR FIND_IN_SET('$t', groups) > 0";
	}

	$res = $dbc->query("SELECT login, name, surname FROM ".TBL_USER.
					   " WHERE active = '1' AND ($qry) ORDER BY login ASC");

	while (list($login, $name, $surname) = $dbc->fetchRow($res)) {
		echo "<h2>Plan użytkownika $login ($name $surname)</h2>\n";
		show_schedule($login, false);
	}
}


class Entry
{
	var $pri;
	var $blk_users;

	function Entry()
	{
		$this->pri = 0;
		$this->blk_users = array();
	}
}

function cmd_cmp()
{
	global $ssn, $dbc, $days;

	$grps = chk_grps();	
	if (count($grps) == 0) {
		if ($_REQUEST["users"] == "") {
			return;
		}
		$users = explode(",",$_REQUEST["users"]);
		$mygrps = $dbc->getGroups($ssn->getLogin());
		$qry = "0";
		foreach ($users as $user) {
			if (count(array_intersect($mygrps, $dbc->getGroups($user))) > 0) {
				$qry .= " OR u.login = '$user' ";
			}
		}
	} else {
		$qry = "0";
		foreach ($grps as $t) {
			$t = $dbc->escape($t);
			$qry .= " OR FIND_IN_SET('$t', groups) > 0";
		}
	}

	/* Initialize the events array */
	$events = array();
	$rowspan = array();
	for ($i = 0; $i < 7; $i++) {
		for ($j = 0; $j < (TIME_END - TIME_START)/TIME_RES; $j++) {
			$events[$i][$j] = new Entry();
		}
		$rowspan[$i] = 0;
	}

	$users = array();

	$res = $dbc->query("SELECT s.login, s.pri, s.day, ".
					   "FLOOR((HOUR(s.start) * 60 + MINUTE(s.start) - ".TIME_START.")/".TIME_RES."),".
					   "CEIL((HOUR(TIMEDIFF(s.end,s.start)) * 60 + MINUTE(TIMEDIFF(s.end,s.start)))/".TIME_RES.") FROM ".
					   TBL_USER." AS u, ".TBL_SCHED." AS s WHERE ".
					   "($qry) && u.login = s.login && u.active = 1 ".
					   "ORDER BY s.start, s.day");

	while (list($login, $pri, $day, $start, $len) = $dbc->fetchRow($res)) {
		$users["$login"] = 1;

		for ($i = 0; $i < $len; $i++) {
			if ($pri == -4) {
				$events[$day][$i + $start]->pri = -9999;
				if (!in_array($login, $events[$day][$i + $start]->blk_users)) {
					array_push($events[$day][$i + $start]->blk_users, $login);
				}	
			} else if ($events[$day][$i + $start]->pri != -9999) {
				$events[$day][$i + $start]->pri += $pri;
			}	
		}
	}

	echo '<table class="schedule">'."\n";
	echo '<colgroup style="width: 9%"><col /></colgroup>'."\n";
	echo '<colgroup span="7"><col class="desc" /><col class="desc" /><col class="desc" />';
	echo '<col class="desc" /><col class="desc" /><col class="desc" /><col class="desc" />';
	echo "</colgroup>\n";
	echo "<tr><th>czas</th>\n";
	foreach ($days as $day) {
		echo "\t<th>$day</th>\n";
	}
	echo "</tr>\n";

	$cnt = count(array_keys($users));

	for ($time = TIME_START, $row = 0; $time < TIME_END; $time += TIME_RES, $row++) {
		for ($i = 0; $i < count($days); $i++) {
			$pri = $events[$i][$row]->pri;
			if ($pri != -9999) {
				$pri = round($pri/$cnt);
			} else {
				$pri = -4;
			}
			$events[$i][$row]->pri = $pri;
		}
	}

	for ($time = TIME_START, $row = 0; $time < TIME_END; $time += TIME_RES, $row++) {
		/* Timeslot description -- the leftmost column.
		 * Each cell has the ID: desc<login>_<rownum> */
		echo '<tr>'."\n\t".'<td class="desc" id="desc'."${login}_$row".'" nowrap>';
		echo time_human($time)." - ".time_human($time + TIME_RES);
		echo "</td>\n";

		for ($i = 0; $i < count($days); $i++) {
			if ($rowspan[$i] > 0) {
				$rowspan[$i]--;
				continue;
			}

			$pri = $events[$i][$row]->pri;
			$blk =& $events[$i][$row]->blk_users;
			$rs = 0;

			for ($j = $row + 1; 
				 $events[$i][$j]->pri == $pri && $events[$i][$j]->blk_users == $blk && $j < (TIME_END-TIME_START)/TIME_RES;
				 $j++) {
				$rs++;
			}
 
			$rowspan[$i] = $rs;

			/* JavaScript functions to hilight the timeslot descriptions
			 * when the mouse cursor is placed over an event */
//					$js = 'onmouseover="descHlght('."'$login',$row,".($row+$rowspan-1).',true)" '.
//						  ' onmouseout="descHlght('."'$login',$row,".($row+$rowspan-1).',false)"';

			echo "\t";
			echo '<td class="pri'.($pri + 4).'"';
			if ($rs > 0) {
				echo ' rowspan="'.($rs+1).'"';
			}
			if ($pri == -4) {
				echo ' title="'.implode(', ',$blk).'">'.count($blk);
			} else {
				echo ">&nbsp;";
			}
			echo "</td>\n";
		}
		echo "</tr>\n";
	}
	echo '</table>';

	echo '<p>Cyfry w czerwonych polach oznaczają liczbę osób, którym dany termin nie pasuje.<br \>';
	echo 'Po najechaniu myszką na wybrane pole można w tooltipie zobaczyć dokładną listę loginów.</p>';

}

function time_human($time)
{
	return sprintf("%02d:%02d",floor($time / 60),($time % 60));
}

	$html->stdFooter(); 

/* vim: set ts=4 encoding=utf-8 nowrap: */
?>
