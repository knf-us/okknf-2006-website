<?php
	include_once("common.php");
	
	$html->stdHeader("Obsługa użytkowników");

	switch ($_REQUEST["cmd"]) {
	case "login":
		cmd_login();
		break;

	case "logout":
		cmd_logout();
		break;

	case "finish":
		cmd_finish();
		break;

	case "register":
		cmd_register();
		break;
	
	case "edit":
		cmd_edit();
		break;
	}


function cmd_logout()
{
	global $dbc, $html, $ssn;

	if (!$ssn->logout()) {
		$html->errorMsg("Wylogowanie nie powiodło się.");
		$html->infoLink(ROOT_URI.'/index.php', "Przejście do strony logowania");
		return;
	}

	$html->infoMsg("Zostałeś(-aś) wylogowany(-a).");
	$html->infoLink(ROOT_URI.'/index.php', "Przejście do strony logowania");
}

function cmd_login() 
{
	global $dbc, $html, $ssn;

	if ($ssn->loggedIn()) {
		$html->errorMsg("Jestes już zalogowany(-a) do systemu.");
		$html->infoLink(ROOT_URI.'/index.php', "Przejście do strony głównej");
		return;
	}

	$lf = new LoginForm($html, $dbc);

	if (!$lf->validate($_REQUEST)) {
		$lf->printForm();
		return;
	}
	
	if (!$ssn->login($_REQUEST["login"])) {
		$html->errorMsg("Logowanie nie powiodło się.");
		$html->infoLink(ROOT_URI.'/index.php', "Przejście do strony logowania");
		return;
	}

	$html->infoMsg("Logowanie zakończone pomyślnie.");
	$html->infoLink(ROOT_URI.'/index.php', "Przejście do strony głównej");

	/* Redirect the user to his homepage in the system.
	 * This will work since output buffering is enabled by default. */
	header("Refresh: 0; URL=".ROOT_URI."/index.php");
}

function cmd_finish()
{
	global $dbc, $html;

	if (!$dbc->activateUser($_REQUEST["login"], $_REQUEST["code"])) {
		$html->errorMsg("Twoje konto nie zostało aktywowane.");
	} else {
		$html->infoMsg("Twoje konto zostało aktywowane.");
		$html->infoLink(ROOT_URI.'/index.php', "Przejście do strony logowania");
	}
}

function cmd_edit()
{
	global $dbc, $html;

	$rf = new RegForm($html, $dbc, "edit");

	if (!$rf->validate($_REQUEST)) {
		$rf->printForm();
		return;
	}

	$html->sectHeader("Informacja");
	$html->infoMsg("Twoje dane *NIE* zostały zmienione.");
	$html->infoMsg("W celu zmiany danych lub ustawień proszę kontaktować się z organizatorami.");
	$html->infoLink(ROOT_URI.'/index.php', "Przejście do strony głównej");

}
	
function cmd_register()
{
	global $dbc, $html;

	$rf = new RegForm($html, $dbc);

	if (!$rf->validate($_REQUEST)) {
		$rf->printForm();
		return;
	}
	
	$confcode = md5(rand()."abc123");
	$login = $_REQUEST["login"];

	$dbc->addUser($_REQUEST, $confcode);

	$subject = 'Konferencja Piknik Naukowy 2006 -- rejestracja';
	$message = "Witaj!\n\nAby dokończyć rejestrację, otwórz poniższą stronę:\n".
			   ROOT_URI."/user.php?cmd=finish&login=${login}&code=${confcode}\n\n".
			   "Dziękujemy za korzystanie z naszego systemu.\n";
	$headers = 'From: '.ADMIN_NAME." <".ADMIN_EMAIL.">\r\n".
			   'Reply-To: '.ADMIN_EMAIL."\r\n".
			   'X-Mailer: PHP';

	$html->sectHeader("Udana rejestracja");

	if (mail($_REQUEST["email"], $subject, $message, $headers)) {
		$html->infoMsg("Twoje zgłoszenie zostało przyjęte, a na podany adres e-mail ".
					   "została wysłana wiadomość z kodem potwierdzającym.");
	} else {
		$html->errorMsg("Wysłanie maila potwierdzającego nie powiodło się. Skontaktuj się ".
					    "z administratorem w celu aktywacji konta.");
	}	

	/* FIXME */
	
	$subject = 'Konferencja 2006 -- nowy użytkownik';
	$message = "Witaj!\n\nNa konferencję właśnie zarejestrowała się nowa osoba,\n".
			   "o loginie '$login'.\n";
	$headers = 'From: '.ADMIN_NAME." <".ADMIN_EMAIL.">\r\n".
			   'Reply-To: '.ADMIN_EMAIL."\r\n".
			   'X-Mailer: PHP';

	mail("spock@gentoo.org", $subject, $message, $headers);

	$html->infoMsg("Po aktywacji konta możliwe będzie zalogowanie się do systemu.");
}

	$html->stdFooter(); 

	/* vim: set ts=4 sts=4 enc=utf-8 nowrap: */
?>
